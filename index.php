<body>
	<div class="wrapper">
		<div class="index-box-img">
			<div class="box-img-h1">
				Создавать уникальную упаковку – наша профессия 
			</div>
			<div class="box-img-button">
				<a href="/template/file/tara-upakovka.pdf" download class="img-button-prez">
					<span class="img-button-prez__text">Скачать презентацию</span>
					<svg aria-hidden="true" width="25" height="25" class="img-button-prez__icon">
						<use xlink:href="/template/img/all.svg#head_baner_play"></use>
					</svg>
				</a>
			</div>
		</div>

		<div class="index-box-advantages-mobile js-advantages-slider">
			<div class="advantages-dec-item advantages-dec-item--first">
				<svg aria-hidden="true" width="60" height="60">
					<use xlink:href="/template/img/all.svg#advantages_factory_mobile"></use>
				</svg>
				<div class="advantages-dec-item__text">Собственное производство</div>
			</div>
			<div class="advantages-dec-item">
				<svg aria-hidden="true" width="60" height="60">
					<use xlink:href="/template/img/all.svg#advantages_1_m"></use>
				</svg>
				<div class="advantages-dec-item__text">Полный цикл производства</div>
			</div>
		</div>
		
		<div class="about-box">
		  <div class="about-box__left">
			<div class="about-box__level1">
			  О Компании
			</div>
			<div class="about-box__level2">
			  Компания <span class="orange">«Яснополянская фабрика тары и упаковки»</span> является одним из ведущих предприятий в России, с опытом работы более 20 лет.
			</div>
			<div class="about-box__level3">
			  <div class="about-box__level3-item">
				<span class="fat-text">1 000 т</span>
				<span class="normal-text">Картона перерабатывается в месяц</span>
			  </div>
			  <div class="about-box__level3-item">
				<span class="fat-text">800 +</span>
				<span class="normal-text">Постоянных клиентов</span>
			  </div>
			  <div class="about-box__level3-item">
				<span class="fat-text">400</span>
				<span class="normal-text">Сотрудников в компании</span>
			  </div>
			</div>
			<div class="about-box__level4">
			  <div class="about-box__level4-but">
				<a href="/about.html">Подробнее</a>
			  </div>
			</div>

			<div class="about-box__level5">
			  <div class="about-box__left-icon">
				<svg class="var1" width="61" height="61" viewBox="0 0 61 61" fill="none" xmlns="http://www.w3.org/2000/svg">
				  <path d="M52.1493 51.0708V9.08134H58.1599V3.09192H52.1493H46.1599V57.0813H58.1599V51.0708H52.1493Z" fill="#495648" fill-opacity="0.7"/>
				  <path d="M21.906 3.09192V57.0813H27.8954V33.0813H33.8848V57.0813H39.8743V3.09192H21.906ZM27.8954 27.0919V9.1025H33.8848V27.0919H27.8954Z" fill="#495648" fill-opacity="0.7"/>
				  <path d="M15.8953 9.08134V3.09192H3.89526V57.0813H15.8953V51.0708H9.90585V33.0813H15.8953V27.0919H9.90585V9.08134H15.8953Z" fill="#495648" fill-opacity="0.7"/>
				</svg>
			  </div>
			</div>
		  </div>

		  <div class="about-box__right">
			<div class="about-box__right-item">
			  Выпущенной картонной трубой можно обогнуть экватор более, чем в 3 раза
			</div>
			<div class="about-box__right-item">
			  Единственное производство в России одноразовый картонной опалубки длиной до 9 метров и диаметром до 1000 мм
			</div>
			<div class="about-box__right-item">
			  С момента основания предприятия объем производства возрос в 4 раза
			</div>
		  </div>
		</div>
		
		<div class="index-infa center">
		  <div class="index-infa__left">
			<div class="index-infa__left-box">
			</div>
		  </div>
		  <div class="index-infa__right">
			<div class="index-infa__right-zag">
			  <h1 class="index-infa__right-zag-h1">Изготовление картонной упаковки на заказ</h1>
			</div>
			<div class="index-infa__right-text">
			  <p>Мы работаем в </p>
			  <div class="index-infa__mobile-hidden-text">
				<p>Учитываем потребности клиента,</p>
			  </div>
			  <span class="index-infa__js-open-hidden-text">	Читать весь текст</span>	
			</div>
			<div class="index-infa__right-but">
			  <a href="/#top_form">
				<div class="but">Получить расчёт</div>
			  </a>
			</div>
		  </div>
		</div>

		<div class="trust-us center">
		  <div class="trust-us__zag">
			Нам доверяют 
		  </div>
		  <div class="trust-us__slide">
			<div class="trust-us__slide-item" style="background-image: url(/template/img/index/trust_us/image_1.png);">
			</div>
			<div class="trust-us__slide-item" style="background-image: url(/template/img/index/trust_us/image_2.png);">
			</div>
			<div class="trust-us__slide-item" style="background-image: url(/template/img/index/trust_us/image_3.png);">
			</div>
		  </div>
		</div>

		<div class="news-index-item">
		  <div class="news-index-item__left">
			<div class="news-index-item__left-img" style="background-image: url(assets/cache/images/dsc_5255-x-4c3.jpg);">
			</div>
		  </div>
		  <div class="news-index-item__right">
			<div class="news-index-item__right-box">
			  <div class="news-index-item__right-data">
				<span class="news-index-item__data">06/09/2022 г.</span>
			  </div>
			  <div class="news-index-item__right-zag">
				Журналисты Тульского телевидения на нашей фабрике
			  </div>
			  <div class="news-index-item__right-text">
				<a href="news/zhurnalisty-tulskogo-televideniya-na-nashej-fabrike.html">Подробнее</a>
			  </div>
			</div>
		  </div>
		</div>



		<div class="news-index-item">
		  <div class="news-index-item__left">
			<div class="news-index-item__left-img" style="background-image: url(assets/cache/images/assets/snippets/phpthumb/noimage-x-5b1.png);">
			</div>
		  </div>
		  <div class="news-index-item__right">
			<div class="news-index-item__right-box">
			  <div class="news-index-item__right-data">
				<span class="news-index-item__data">24/07/2019 г.</span>
			  </div>
			  <div class="news-index-item__right-zag">
				Кашированные коробки: жесткая и необычная картонная упаковка
			  </div>
			  <div class="news-index-item__right-text">
				Технология каширования превращает стандартную картонную коробку в стильную, эффектную упаковку. Нанесение дополнительного слоя открывает новые возможности в оформлении и позволяет придать конструкции необходимую прочность и жесткость.
			  </div>
			  <div class="news-index-item__right-but">
				<a href="news/kashirovannye-korobki-jeskaya-i-neobychnaya-kartonnaya-upakovka.html">Подробнее</a>
			  </div>
			</div>
		  </div>
				В августе фабрика отметила юбилей генерального директора. По этому случаю на предприятии побывали журналисты тульского телевидения.
			  </div>
			  <div class="news-index-item__right-but">
		</div>


		<div class="bottom_but"><a href="/news.html">Смотреть все</a></div>
	</div>
</body>